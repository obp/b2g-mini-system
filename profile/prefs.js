user_pref("b2g.system_startup_url", "chrome://system/content/index.html");
user_pref("b2g.multiscreen.system_remote_url", "chrome://system/content/index.html");
// forkserver is broken on Alpine
user_pref("dom.ipc.forkserver.enable", false);
// Make typing passwords easier
user_pref("editor.password.mask_delay", 200);
// Allow debugging via USB network and Wi-Fi
user_pref("devtools.debugger.force-local", false);
// Tell Gecko to use Wayland's input method instead of B2G's
user_pref("dom.inputmethod.enabled", false);
