#!/bin/sh -e

webappsdir="$PWD/profile/webapps"

mkdir -p "$webappsdir"
echo ">>> generating webapps.json"
./generate-webapps-json.sh > "$webappsdir/webapps.json"

cd apps
for app in *; do
	echo ">>> packing $app"
	mkdir -p "$webappsdir/$app"
	rm -f "$webappsdir/$app/application.zip"
	(cd "$app" && zip -r -9 "$webappsdir/$app/application.zip" .)
done
