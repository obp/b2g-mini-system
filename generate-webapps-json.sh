#!/bin/sh

getmd5sum () {
	md5sum "$1" | sed 's/[^0-9a-z].*$//'
}

appentry () {
	name=$1
	cat <<END
  {
    "name": "$name",
    "manifest_url": "http://$name.localhost/manifest.webmanifest",
    "install_time": $(date +%s)000,
    "manifest_hash": "$(getmd5sum "$name/manifest.webmanifest")"
  },
END
}

echo "["
cd apps
for app in *; do
	appentry "$app"
done | sed -z 's/,[^,]*$//'
echo
echo "]"
