export default class Tab extends EventTarget {
  constructor(uri, isApp, openWindowInfo) {
    super();
    this.isApp = isApp;
    this.wv = document.createElement('web-view');
    this.wv.openWindowInfo = openWindowInfo;
    this.wv.setAttribute('remote', 'true');
    this.wv.setAttribute('src', uri);;
    this.wv.className = 'tab-inactive';
    let handlers = {
      'close': () => {
        this.dispatchEvent(new CustomEvent('tabclosed'));
        console.log(`-*- System: app closed`);
      },
      'titlechange': (e) => {
        console.log(`-*- System: title change`, e.detail);
      },
      'securitychange': (e) => {
        console.log(`-*- System: security change`, e.detail);
      },
      'locationchange': (e) => {
        console.log(`-*- System: location change`, e.detail);
      },
      'manifestchange': (e) => {
        console.log(`-*- System: manifest change`, e.detail);
      },
      'showmodalprompt': (e) => {
        console.log(`-*- System: prompt`, e.detail);
        e.detail.unblock({});
      },
      'backgroundcolor': (e) => {
        console.log(`-*- System: background color`, e.detail);
      },
      'loadstart': () => {
      },
      'loadend': () => {
      },
      'processready': () => {
      },
      'promptpermission': (e) => {
        console.log(`-*- System: permission prompt`, e.detail);
        this.wv.dispatchEvent(new CustomEvent(e.detail.requestId, {
          detail: {
            choices: {},
            origin: e.detail.origin,
            granted: true,
            remember: false
          }
        }));
      },
    };
    for(const [ key, handler ] of Object.entries(handlers)){
      this.wv.addEventListener(key, handler);
    }
    if (isApp) {
      fetch(uri)
        .then((req) => req.json())
        .then((json) => {
          let url = new URL(json.start_url || "/", uri);
          this.wv.setAttribute('src', url.href);
        })
        .catch((e) => {
          console.log('-*- System: manifest load error', e);
        });
    } else {
      this.wv.setAttribute('src', uri);
    }
  }
  goBack() {
    if (this.wv.canGoBack) {
      this.wv.goBack();
      return true;
    } else {
      return false;
    }
  }
  deactivate() {
    this.wv.className = 'tab-inactive';
  }
  activate() {
    this.wv.className = 'tab-active';
    this.wv.focus();
  }
};
