import Embedder from './embedder.js';
import TabManager from './tabmanager.js';

let manager = new TabManager();
let embedder = new Embedder(manager);

embedder.start();
navigator.b2g.setDispatchKeyToContentFirst(true);
window.addEventListener('keydown', (e) => {
  switch (e.key) {
    case "Backspace":
      if (e.defaultPrevented) {
        console.log('Backspace pressed: app prevented close');
      } else {
        console.log('Backspace pressed: system closing app');
        manager.back();
      }
      break;
    case "EndCall":
      console.log('EndCall pressed: system closing app');
      manager.back();
      break;
  }
});
manager.newTab("https://abscue.de", false, null);
