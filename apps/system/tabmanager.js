import Tab from './tab.js';

export default class TabManager {
  constructor() {
    this.tabstack = [];
    this.container = document.createElement('div');
    this.container.className = 'tab-manager';
    document.body.appendChild(this.container);
  }
  removeTab = () => {
    console.log('removing tab');
    let tab = this.activeTab;
    tab.removeEventListener('tabclosed', this.removeTab);
    tab.deactivate();
    setTimeout(() => tab.wv.remove(), 1000);
    this.activeTab = this.tabstack.pop();
    if (this.activeTab) {
      this.activeTab.addEventListener('tabclosed', this.removeTab);
      this.activeTab.activate();
    }
  }
  back() {
    if (this.activeTab) {
      if (this.activeTab.isApp || !this.activeTab.goBack())
        this.removeTab();
    }
  }
  newTab(uri, isApp, openWindowInfo) {
    let tab = new Tab(uri, isApp, openWindowInfo);
    this.container.appendChild(tab.wv);
    if (this.activeTab) {
      this.activeTab.removeEventListener('tabclosed', this.removeTab);
      this.activeTab.deactivate();
      this.tabstack.push(this.activeTab);
    }
    this.activeTab = tab;
    tab.addEventListener('tabclosed', this.removeTab);
    tab.activate();
  }
};
