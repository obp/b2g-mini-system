function dbg(msg){
  console.log(`-*- Embedder: ${msg}`);
}

export default class Embedder {
  constructor(manager) {
    this.manager = manager;
    this.we = new WebEmbedder({
      windowProvider: {
        openURI: this.w_openURI,
        createContentWindow: this.w_createCW,
        openURIInFrame: this.w_openURIInFrame,
        createContentWindowInFrame: this.w_createCWInFrame,
        isTabContentWindow: this.w_isTabCW,
        canClose: this.w_canClose,
      },
      notifications: {
        //showNotification: this.n_show,
        //closeNotification: this.n_close,
      },
      // imeHandler
      // screenReaderProvider
      activityChooser: {
        choseActivity: this.a_chose,
      },
      processSelector: {
        provideProcess: this.p_provide,
        suggestServiceWorkerProcess: this.p_suggestSW,
      },
    });
  }
  start() {
    this.we.addEventListener('launch-app', this.launchApp);
    //this.we.addEventListener('runtime-ready', () => {});
  }
  open(params, uri) {
    let frame = this.manager.newTab(
      uri,
      params?.features?.split(',').includes('kind=app') ?? false,
      params?.openWindowInfo ?? null
    );
    return { frame };
  }
  launchApp = (e) => {
    this.open({ features: 'noopener,kind=app' }, e.detail.manifestUrl);
  }
  w_openURI = (uri, openWindowInfo, where, flags, tp, csp) => {
    dbg(`openURI ${uri}`);
    throw new Error('NOT IMPLEMENTED');
  }
  w_createCW = (uri, openWindowInfo, where, flags, tp, csp) => {
    dbg(`createCW ${uri}`);
    return this.open({ openWindowInfo }, uri);
  }
  w_openURIInFrame = (uri, params, where, flags, nextRemoteTab, name) => {
    dbg(`openURIInFrame ${uri} ${name}`);
    return this.open(params, uri);
  }
  w_createCWInFrame = (uri, params, where, flags, nextRemoteTab, name) => {
    dbg(`createCWInFrame ${uri} ${name}`);
    return this.open(params, uri);
  }
  w_isTabCW = (win) => false
  w_canClose = (win) => true
  p_provide = (type, processes, maxcount) => -1
  p_suggestSW = (scope) => 0
  a_chose = async (a) => {
    dbg(`choseActivity ${a.id} ${a.name}`);
    for (let i = 0; i < a.choices.length; i++) {
      dbg(`choice ${i} is ${a.choices[i].manifest}`);
    }
    return {
      id: a.id,
      value: '0'
    };
  }
}
